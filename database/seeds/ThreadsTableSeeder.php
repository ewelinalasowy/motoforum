<?php

use Illuminate\Database\Seeder;
use App\Thread;

class ThreadsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $data =
            [
                [
                    'id' => 1,
                    'user_id' => 1,
                    'title' => 'Ford Mustang',
                    'slug' => 'ford-mustang',
                    'body' => 'Ford Mustang is cool!',
                    'category_id' =>1

                ],

                [
                    'id' => 2,
                    'user_id' => 2,
                    'title' => 'Volkswagen Beetle',
                    'slug' =>'volkswagen-beetle',
                    'body' => 'VW Beetle is soooooooo good!',
                    'category_id' =>2
                ],

            ];
        foreach($data as $row) {
            $model = Thread::firstOrNew(["id" => $row["id"]]);
            $model->fill($row);
            $model->save();
        }
    }
}
