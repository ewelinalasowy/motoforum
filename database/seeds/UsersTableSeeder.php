<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        //


        $data =
            [
                [
                    'id' => 1,
                    'name' => 'admin',
                    'email' => 'admin@example.com',
                    'email_verified_at' => Carbon::createFromDate(2019, 04, 05)->toDateTimeString(),
                    'password' => Hash::make('admin123'),
                    'role_id' => '1',
                    'avatar_name' => 'nophoto.png',

                ],
                [
                    'id' => 2,
                    'name' => 'user',
                    'email' => 'user@example.com',
                    'email_verified_at' => Carbon::createFromDate(2019, 04, 05)->toDateTimeString(),
                    'password' => Hash::make('user123'),
                    'role_id' => '2',
                    'avatar_name' => 'nophoto.png',

                ],
            ];
        foreach ($data as $row) {
            $model = User::firstOrNew(["id" => $row["id"]]);
            $model->fill($row);
            $model->save();

        }
    }
}
