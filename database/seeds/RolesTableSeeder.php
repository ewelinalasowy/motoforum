<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $data =
            [
                [
                    'id' => 1,
                    'description' => 'admin',

                ],

                [
                    'id' => 2,
                    'description' => 'user',
                ],

                [
                    'id' => 3,
                    'description' => 'banned',
                ],

            ];
        foreach($data as $row) {
            $model = Role::firstOrNew(["id" => $row["id"]]);
            $model->fill($row);
            $model->save();
        }
    }
}
