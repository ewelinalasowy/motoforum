<?php

use Illuminate\Database\Seeder;
use App\Reply;

class RepliesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $data =
            [
                [
                    'id' =>1,
                    'thread_id' => 1,
                    'user_id' => 2,
                    'body' => 'Ford Mustang is awesome!'

                ],

                [
                    'id' =>2,
                    'thread_id' => 2,
                    'user_id' => 1,
                    'body' => 'VW Beetle is beautiful!'
                ],

                [
                    'id' =>3,
                    'thread_id' => 1,
                    'user_id' => 1,
                    'body' => 'Ford Mustang is spectacular!'

                ],


            ];
        foreach($data as $row) {
            $model = Reply::firstOrNew(["id" => $row["id"]]);
            $model->fill($row);
            $model->save();
        }

    }
}
