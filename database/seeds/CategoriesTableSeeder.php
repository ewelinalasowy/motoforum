<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //


        $data =
            [
                [
                    'id' => 1,
                    'name' => 'Ford Mustang',
                    'slug'=>'ford-mustang',

                ],

                [
                    'id' => 2,
                    'name' => 'Volkswagen Beetle',
                    'slug' => 'volkswagen-beelte',
                ],

                [
                    'id' => 3,
                    'name' => 'Opel Astra G',
                    'slug' => 'opel-astra-g',
                ],

                [
                    'id' => 4,
                    'name' => 'Skoda Fabia',
                    'slug' => 'skoda-fabia',
                ],

                [
                    'id' => 5,
                    'name' => 'Ferrari F40',
                    'slug' => 'ferrari-f40',
                ],


            ];
        foreach($data as $row) {
            $model = Category::firstOrNew(["id" => $row["id"]]);
            $model->fill($row);
            $model->save();
        }
    }
}
