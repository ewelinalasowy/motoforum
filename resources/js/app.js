
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app'
});


window.Pusher = require('pusher-js');
import Echo from "laravel-echo";

window.Echo = new Echo({
    broadcaster: 'pusher',
    key: 'f2378789a476020c0ca9',
    cluster: 'eu',
    encrypted: true
});

const NOTIFICATION_TYPES = {
    follow: 'App\\Notifications\\UserFollowed',
    newThread: 'App\\Notifications\\NewThread',
    threadWasUpdated: 'App\\Notifications\\ThreadWasUpdated',
};
require('./bootstrap');

var notifications = [];



$(document).ready(function() {
    // check if there's a logged in user
    if(Laravel.userId) {
        $.get('/notifications', function (data) {
            addNotifications(data, "#notifications");
        });

        window.Echo.private(`App.User.${Laravel.userId}`)
            .notification((notification) => {
                addNotifications([notification], '#notifications');
            });
    }
});

function addNotifications(newNotifications, target) {
    notifications = _.concat(notifications, newNotifications);
    // show only last 5 notifications
    notifications.slice(0, 5);
    showNotifications(notifications, target);
}

function showNotifications(notifications, target) {
    if(notifications.length) {
        var htmlElements = notifications.map(function (notification) {
            return makeNotification(notification);
        });
        $(target + 'Menu').html(htmlElements.join(''));
        $(target).addClass('has-notifications')
    } else {
        $(target + 'Menu').html('<li class="dropdown-header">No notifications</li>');
        $(target).removeClass('has-notifications');
    }
}

// Make a single notification string
function makeNotification(notification) {
    var to = routeNotification(notification);
    var notificationText = makeNotificationText(notification);
    return '<li><a href="' + to + '">' + notificationText + '</a></li>';
}

// get the notification route based on it's type
function routeNotification(notification) {

    var to = '?read=' + notification.id;
    if(notification.type === NOTIFICATION_TYPES.follow) {
        return '/home'+to;
    }
 else if(notification.type === NOTIFICATION_TYPES.newThread || notification.type === NOTIFICATION_TYPES.threadWasUpdated ) {
    var temp = 'threads/'+notification.data.thread_category+'/'+notification.data.thread_slug;
}


    return '/'+temp+to;
}

// get the notification text based on it's type
function makeNotificationText(notification) {
    var text = '';
    if(notification.type === NOTIFICATION_TYPES.follow) {
        const name = notification.data.follower_name;
        text += '<strong>' + name + '</strong> followed you';
    }
    else if(notification.type === NOTIFICATION_TYPES.newThread) {
    const name = notification.data.following_name;
    text +=  '<strong>' + name + '</strong> published a new thread';
}
    else if(notification.type === NOTIFICATION_TYPES.threadWasUpdated)
    {
        const name = notification.data.thread_title;
        text +=  '<strong>' + name + '</strong> has a new reply!';
    }

    return text;
}

