@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">

                <div class="page-header">
                    <h1>
                        {{ $profileUser }}
                    </h1>
                </div>

                        <img src="/storage/avatars/{{ $avatar }}"  width="150" height="150"/>

                @if (Auth::check())
                    <form method="POST" action="/profiles/show/{{$profileUser}}/follow">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">@lang('profiles_show.follow')</button>
                        </div>

                    </form>

                    <form method="POST" action="/profiles/show/{{$profileUser}}/unfollow">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">@lang('profiles_show.unfollow')</button>
                        </div>

                    </form>
                @else
                    <p class="text-center">@lang('profiles_show.please')<a href="{{ route('login') }}">@lang('profiles_show.sign_in')</a>@lang('profiles_show.to_follow_user')</p>

                @endif
                @foreach ($threads as $thread)
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="level">

                                @lang('activities.at')   <span>{{ $thread->created_at}}</span>
                                <div class="panel-body">
                                    <p class="font-weight-bold"> @lang('activities.published')  <a href="{{$thread->path()}}">{{$thread->title}} ({{ $thread->replies_count }})</a> </p>
                                </div>

                            </div>
                        </div>


                    </div>
                @endforeach

            </div>
            </div>


        {{ $threads->links() }}
        </div>
    </div>


@endsection