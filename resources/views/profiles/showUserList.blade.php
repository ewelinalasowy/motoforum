@extends('layouts.app')
@section('content')



    <div class="row">
        <div class="col-sm-1">
        </div>
        <div class="col-sm-9">
            <h1>@lang('profiles_showUserList.registered_users')</h1>
            <table class="table">
                <thead class="thead-light">
                <tr>
                    <th scope ="col"> @lang('profiles_showUserList.id')</th>
                    <th scope ="col">@lang('profiles_showUserList.name')</th>
                    <th scope ="col">@lang('profiles_showUserList.mail')</th>
                    <th scope ="col">@lang('profiles_showUserList.activity')</th>
                    <th scope ="col">@lang('profiles_showUserList.role')</th>
                    <th scope ="col">@lang('profiles_showUserList.delete')</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($userList as $user)
                    <tr>
                        <td>{{ $user->id}}</td>
                        <td>{{ $user->name}}</td>
                        <td>{{ $user->email}}</td>
                        <td><a href="{{route('profile',$user->name)}}">@lang('profiles_showUserList.profile')</a></td>
                        @can('update',$user)
                            <td>
                                <form action="/manage/users/role" method="post">
                                    {{ csrf_field() }}
                                    {{ method_field('patch') }}
                                    <div class="form-group{{ $errors->has('role_id') ? ' has-error' : '' }}">
                                        <select name="role_id" class="form-control col-10">
                                            @foreach($roles as $data)
                                                @if($data->id == $user->role_id)
                                                    <option value="{{$data->id}}"
                                                            selected> {{$data->description}}</option>
                                                @else
                                                    <option value="{{$data->id}}"> {{$data->description}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        @if($errors->has('role_id'))
                                            <span class="help-block">{{ $errors->first('role_id') }}</span>
                                        @endif
                                        <input type="hidden" name="UID" value="{{$user->id}}"/>
                                        <button type="submit" class="btn btn-primary">@lang('profiles_showUserList.submit')</button>
                                    </div>
                                </form>
                            </td>
                        @endcan
                        @cannot('update', $user)
                            <td>@lang('profiles_showUserList.owner')</td>
                        @endcannot
                        <td>
                            @can('delete', $user)
                                <form action="{{ url('/manage/users/delete', ['id' => $user->id]) }}" method="post"
                                      class="delete">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                    <button class="btn btn-danger" type="submit"
                                            onclick="return confirm('Are you sure to delete this user?')">
                                        <i class="fas fa-trash-alt"></i> @lang('profiles_showUserList.delete')
                                    </button>
                                </form>
                            @endcan
                        </td>
                    </tr>
                @endforeach
                </tbody>

            </table>
        </div>



        <div class="col-sm-1">
            <div class="titleOfRoles">
                <h3>@lang('profiles_showUserList.list_of_roles')</h3>
                <ul class="list-group" >
                    <li class="list-group-item">
                        <a href="{{route('Manage users')}}">@lang('profiles_showUserList.all')</a>
                    </li>
                    @foreach ($roles as $role)
                        <li class="list-group-item">
                            <a href="{{route('Show users by role',$role->id) }}">{{$role->description}}</a>
                        </li>
                    @endforeach
                </ul>
            </div>




        </div>
        <div class="col-sm-1">

        </div>
    </div>





@endsection