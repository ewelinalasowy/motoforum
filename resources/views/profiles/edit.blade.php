@extends('layouts.app')

@section('content')
    <div class="container">
        <h1><b>@lang('profiles_edit.edit')</b></h1>
        <div class="edit-form">
            <form method="POST" action="/profiles/edit" enctype="multipart/form-data">
                <div class="form-group hidden">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="_method" value="PATCH"> {{ method_field('PATCH') }}
                </div>
                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="email" class="control-label"><b>@lang('profiles_edit.name')</b></label>
                    <input type="text" name="name" placeholder=@lang('profiles_edit.name_here') class="form-control"
                           value="{{ $user->name }}"/>

                     @if ($errors->has('name'))
                    <span class="help-block">
            <strong>{{$errors->first('name')}}</strong>
        </span>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="control-label"><b>@lang('profiles_edit.mail')</b></label>
                    <input type="text" name="email" placeholder=@lang('profiles_edit.mail_here') class="form-control"
                           value="{{ $user->email }}"/>
                    @if ($errors->has('email'))
                    <span class="help-block">
            <strong>{{$errors->first('email')}}</strong>
        </span>
                    @endif

                    <label for="avatar_name" class="control-label"><b>Avatar:</b></label>
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="avatar_name" name="avatar_name" value="{{ old('avatar_name') }}">
                        <label class="custom-file-label" for="avatar_name">Choose file</label>
                    </div>

                    <script type="application/javascript">
                        $('input[type="file"]').change(function (e) {
                            var fileName = e.target.files[0].name;
                            $('.custom-file-label').html(fileName);
                        });
                    </script>


                        <div class="form-group">
                        <button type="submit" class="btn btn-primary">@lang('profiles_edit.submit')</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection