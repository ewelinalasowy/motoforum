@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Update reply</div>

                <div class="panel-body">
                    <form action="/threads/{{$thread->category->slug}}/{{$thread->slug}}/replies/{{$reply->id}}/edit" method="POST">

                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}
                        <div class="form-group {{ $errors->has('body') ? ' has-error' : '' }}">
                            <label for="body" class="control-label"><b>@lang('threads_create.body')</b></label>
                            <textarea name="body" id="body" class="form-control"
                                      rows="8" required> {{$reply->body}}</textarea>

                            @if ($errors->has('body'))
                                <span class="help-block">
                                     <strong>{{$errors->first('body')}}</strong>
                                        </span>
                            @endif
                        </div>


                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>

                        @if (count($errors))
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection