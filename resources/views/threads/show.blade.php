@extends('layouts.app')

@section('content')
    <h2 class="text-center">{{$thread->title}}</h2>

    <div class="card mb-3">

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3 bg-light">
                    <p class="text-center mt-2">
                        <img src="/storage/avatars/{{ $thread->user->avatar_name }}"  class="rounded-circle" width="150" height="150"/>
                    </p>
                    <p class="text-center font-weight-bold">
                        <a href="{{ route('profile', $thread->user) }}">{{ $thread->user->name }}</a>
                    </p>
                    <p>
                        @lang('threads_show.number_of_replies') {{ $thread->replies_count }}
                    </p>

                    <form method="POST" action="/threads/{{$thread->category->slug}}}/{{$thread->slug}}/subscriptions">
                        {{ csrf_field() }}

                        <button type="submit" class="btn btn-warning" {{ $thread->isSubscribed() ? 'disabled' : '' }}>
                            @lang('threads_show.subscribe')
                        </button>

                    </form>

                    @can ('delete',$thread)
                        <form action="{{ $thread->path() }}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}

                            <button type="submit" class="btn btn-danger">@lang('threads_show.delete_thread')</button>
                        </form>
                    @endcan

                    @can ('update',$thread)

                        <div><a href="/edit/threads/{{$thread->category->slug}}/{{$thread->slug}} "
                                class="btn btn-primary">@lang('threads_show.edit_button')</a></div>

                    @endcan

                </div>
                <div class="col-md-9">
                    <p class="font-weight-bold">{{$thread->title}}, {{$thread->category->name}} </p>
                    <p class="mt-1">
                        <small> {{$thread->created_at}}</small>
                    </p>

                    <hr>
                    <p>
                        {{$thread->body}}
                    </p>

                    <hr>
                    <p class="font-italic">
                        @lang('threads_show.updated_at') {{$thread->updated_at}}
                    </p>


                </div>
            </div>
        </div>

    </div>

    @foreach($replies as $reply)
        <div class="card mb-3">

            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3 bg-light">
                        <p class="text-center mt-2">
                            <img src="/storage/avatars/{{ $reply->user->avatar_name }}" class="rounded-circle" width="150" height="150"/>
                        </p>
                        <p class="text-center font-weight-bold">
                            <a href="{{ route('profile', $reply->user) }}">{{ $reply->user->name }}</a>
                        </p>
                        <p>
                        <form method="POST" action="/replies/{{ $reply->id }}/favorites">
                            {{ csrf_field() }}

                            <button type="submit" class="btn btn-info" {{ $reply->isFavorited() ? 'disabled' : '' }}>
                                @lang('threads_show.favorite')
                                {{ $reply->favorites_count }}
                            </button>
                        </form>
                        </p>

                        @can ('delete', $reply)
                            <div class="panel-footer">
                                <form method="POST" action="/replies/{{ $reply->id }}">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}

                                    <button type="submit"
                                            class="btn btn-danger btn-xs">@lang('threads_show.delete')</button>

                                    @can ('update',$reply)
                                        <div>
                                            <a href="/threads/{{$thread->category->slug}}/{{$thread->slug}}/replies/{{$reply->id}}/edit"
                                               class="btn btn-primary">@lang('threads_show.edit_button')</a></div>
                                    @endcan
                                </form>
                            </div>
                        @endcan

                    </div>
                    <div class="col-md-9">
                        <p class="font-weight-bold">{{$thread->title}}, {{$thread->category->name}} </p>
                        <p class="mt-1">
                            <small> {{$reply->created_at}}</small>
                        </p>

                        <hr>
                        <p>
                            {{$reply->body}}
                        </p>

                        <hr>
                        <p class="font-italic">
                            @lang('threads_show.updated_at') {{$reply->updated_at}}
                        </p>


                    </div>
                </div>
            </div>
            {{ $replies->links() }}
            @endforeach
        </div>


        @if (Auth::check())
            <div class="col-md-8">
                <form method="POST" action="{{ $thread->path() . '/replies' }}">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <textarea name="body" id="body" class="form-control"
                                  placeholder=@lang('threads_show.reply') rows="5"></textarea>
                    </div>

                    <button type="submit" class="btn btn-success">@lang('threads_show.post')</button>

                    @if (count($errors))
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                </form>
                <div>

                </div>
            </div>

        @else
            <p class="text-center">@lang('threads_show.please')
                <a href="{{ route('login') }}">@lang('threads_show.sign_in')</a>@lang('threads_show.participate')
            </p>
        @endif


@endsection
