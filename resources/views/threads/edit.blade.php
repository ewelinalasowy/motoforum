@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Update thread</div>

                    <div class="panel-body">
                            <form action="/edit/threads/{{$thread->category->slug}}/{{$thread->slug}}" method="POST">

                            {{ csrf_field() }}
                                {{ method_field('PATCH') }}
                                <div class="form-group {{ $errors->has('body') ? ' has-error' : '' }}">
                                    <label for="body" class="control-label"><b>@lang('threads_create.body')</b></label>
                                    <textarea name="body" id="body" class="form-control"
                                              rows="8" required> {{$thread->body}}</textarea>

                                    @if ($errors->has('body'))
                                        <span class="help-block">
                                     <strong>{{$errors->first('body')}}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="category_id">@lang('threads_create.choose')</label>
                                    <select name="category_id" id="category_id" class="form-control" required>
                                        <option value="">@lang('threads_create.choose_one')</option>

                                        @foreach ($categories as $category)
                                            <option value="{{ $category->id }}" {{ old('category_id') == $category->id ? 'selected' : '' }}>
                                                {{ $category->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>


                                <div class="form-group">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>

                            @if (count($errors))
                                <ul class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection