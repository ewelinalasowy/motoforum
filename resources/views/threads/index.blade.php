@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
        <div class=" panel panel-default">


            <form action="/search" method="POST" role="search">
                {{ csrf_field() }}
                <div class="input-group">
                    <input type="text" class="form-control" name="q"
                           placeholder="Search threads"> <span class="input-group-btn">
            <button type="submit" class="btn btn-default">
                <span class="fa fa-search"></span>
            </button>
        </span>
                </div>
            </form>


            <div class = "panel-heading"> @lang('threads_index.list_of_threads')

            </div>


                @foreach($threads as $thread)
                    <div> <a href="{{$thread->path()}}">{{$thread->title}} ({{ $thread->replies_count }})</a> </div>

            @endforeach


        </div>
</div>
    </div>
</div>
@endsection
