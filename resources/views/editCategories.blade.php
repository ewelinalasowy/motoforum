@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <h1>@lang('editCategories.add_new_category')</h1>
                <form action="/manage/categories/add" method="post" enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    <div class="form-group{{ $errors->has('name') ? 'has-error' : '' }}">
                        <label for="title">@lang('editCategories.name')</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="@lang('editCategories.name')"
                               value="{{ old('name') }}">
                        @if($errors->has('name'))
                            <span class="help-block">{{ $errors->first('name') }}</span>
                        @endif
                    </div>

                    <button type="submit" class="btn btn-success">
                        <i class="fas fa-plus"></i>@lang('editCategories.add')
                    </button>
                </form>
            </div>

            <div class="col-sm-6">
                <h1>@lang('editCategories.remove_category')</h1>
                <form action="/manage/categories/delete" method="post">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <label for="description">@lang('editCategories.select_category_to_remove')</label><br>
                        <select name="categories" class="form-control">
                            @foreach($categories as $data)
                                <option value="{{$data->id}}"> {{$data->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <button class="btn btn-danger" type="submit"
                            onclick="return confirm('Are you sure to delete this category?')">
                        <i class="fas fa-trash-alt"></i> @lang('editCategories.delete')
                    </button>
                </form>
            </div>

        </div>
    </div>
@endsection