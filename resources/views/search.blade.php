@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
        <div class=" panel panel-default">
            <div class = "panel-heading"> @lang('threads_index.list_of_threads')

    </div>

            <form action="/search" method="POST" role="search">
                {{ csrf_field() }}
                <div class="input-group">
                    <input type="text" class="form-control" name="q"
                           placeholder="Search threads"> <span class="input-group-btn">
            <button type="submit" class="btn btn-default">
                <span class="fa fa-search"></span>
            </button>
        </span>
                </div>
            </form>


            <div class="container">
                @if(isset($details))
                    <p> The Search results for your query <b> {{ $query }} </b> are :</p>
                    <h2>Threads</h2>
                    <table class="table table-striped">
                        <thead>

                        </thead>
                        <tbody>
                        @foreach($details as $thread)
                            <tr>
                                <a href="{{$thread->path()}}">{{$thread->title}} ({{ $thread->replies_count }})
                                </a>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif



            </div>


        </div>
</div>
    </div>
</div>
@endsection
