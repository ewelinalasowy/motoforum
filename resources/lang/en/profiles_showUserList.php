<?php


return [

    'registered_users' => 'List of registered users',
    'id' => 'ID: ',
    'name' => 'Name',
    'mail' => 'E-mail',
    'activity' => 'Activity',
    'role' => 'Role',
    'profile' => 'Show profile',
    'submit' => 'Submit',
    'owner' => 'Owner',
    'delete' => 'Delete',
    'list_of_roles' => 'List of roles',
    'all' => 'All'

];