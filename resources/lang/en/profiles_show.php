<?php


return [

    'follow' => 'Follow',
    'unfollow' => 'Unfollow',
    'please' => 'Please ',
    'sign_in' => ' sign in ',
    'to_follow_user' => ' to follow user.',
    'activities' => 'There is no activity for this user yet.'
];