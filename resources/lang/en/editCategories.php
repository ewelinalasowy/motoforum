<?php


return [

    'add_new_category' => 'Add new category',
    'name' => 'Name',
    'add' => 'Add',
    'remove_category' => 'Remove category',
    'select_category_to_remove' => 'Select category to remove',
    'delete' => 'Delete'

];