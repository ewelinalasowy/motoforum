<?php


return[
    'success_create_thread' => 'Thread has been created successfully!',
    'success_upload_thread' => 'Thread has been updated successfully!',
    'error_delete_thread' => 'Thread has been deleted successfully.',
    'role_update_user' => 'User role has been updated successfully.',
    'deleted_user' => 'User has been deleted successfully',
    'category_added' => 'Category has been added successfully',
    'category_deleted' => 'Category has been deleted successfully',
    'success' => 'Success',
    'follow_yourself' => 'You cannot follow yourself.',
    'friends_with' => 'You are now friends with :user',
    'already_following' => 'You are already following :user',
    'no_friends' => 'You are no longer friends with :user',
    'not_fallowing' => 'You are not following :user',
    'update_reply' =>'Reply has been updated successfully',
    'delete_reply' => 'Reply has been deleted successfully.'
];