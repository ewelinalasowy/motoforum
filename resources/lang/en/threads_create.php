<?php


return [

    'create' => 'Create a new thread',
    'title' => 'Title:',
    'body' => 'Body:',
    'choose' => 'Choose a category:',
    'choose_one' => 'Choose one...',
    'publish' => 'Publish'
];