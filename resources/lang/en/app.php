<?php


return[
    'all_threads'=>'All threads',
    'categories'=>'Categories',
    'browse'=>'Browse',
    'my_threads'=>'My threads',
    'popular_threads'=>'Popular threads',
    'unanswered_threads'=>'Unanswered threads',
    'polish'=>'Polish',
    'english'=>'English',
    'no_notification'=>'No notifications',
    'my_profile'=>'My profile',
    'edit_my_profile'=>'Edit my profile',
    'menage_categories'=>'Manage categories',
    'menage_users'=>'Manage users',
    'add_new_threads'=>'Add new thread'
];