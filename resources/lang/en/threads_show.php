<?php


return [

    'title' => 'Title:',
    'body' => 'Body:',
    'category' => 'Category: ',
    'user' => 'User:',
    'created_at' => 'Created at:',
    'number_of_replies' => 'Number of replies:',
    'delete_thread' => 'Delete thread',
    'subscribe' => 'Subscribe',
    'replies' => 'Replies:',
    'delete' => 'Delete',
    'post' => 'Post',
    'reply' => 'Reply...',
    'please' => 'Please ',
    'sign_in' => ' sign in ',
    'participate' => ' to participate in this discussion.',
    'favorite' => 'Favorite: ',
    'edit_button' => 'Edit',
    'updated_at' => 'Updated at: '

];