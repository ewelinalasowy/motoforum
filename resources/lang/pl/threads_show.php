<?php


return [

    'title' => 'Tytuł:',
    'body' => 'Treść:',
    'category' => 'Kategoria: ',
    'user' => 'Użytkownik:',
    'created_at' => 'Utworzone:',
    'number_of_replies' => 'Liczba odpowiedzi:',
    'delete_thread' => 'Usuń wątek',
    'subscribe' => 'Subskrybuj',
    'replies' => 'Odpowiedzi:',
    'delete' => 'Usuń',
    'post' => 'Wyślij',
    'reply' => 'Odpowiedź...',
    'please' => 'Proszę ',
    'sign_in' => ' zaloguj się ',
    'participate' => ' by uczestniczyć w dyskusji.',
    'favorite' => 'Ulubione: ',
    'edit_button' => 'Edytuj',
    'updated_at' => 'Edytowano: '

];