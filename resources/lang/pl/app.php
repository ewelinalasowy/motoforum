<?php


return[
    'all_threads'=>'Wszystkie wątki',
    'categories'=>'Kategorie',
    'browse'=>'Wyszukaj',
    'my_threads'=>'Moje wątki',
    'popular_threads'=>'Popularne wątki',
    'unanswered_threads'=>'Nie odpowiedziane wątki',
    'polish'=>'Polski',
    'english'=>'Angielski',
    'no_notification'=>'Brak powiadomień',
    'my_profile'=>'Mój profil',
    'edit_my_profile'=>'Edytuj mój profil',
    'menage_categories'=>'Zarządzanie kategoriami',
    'menage_users'=>'Zarządzanie użytkownikami',
    'add_new_threads'=>'Dodaj nowy wątek'
];