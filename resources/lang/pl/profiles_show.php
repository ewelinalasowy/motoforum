<?php


return [

    'follow' => 'Obserwuj',
    'unfollow' => 'Przestań obserwować',
    'please' => 'Proszę ',
    'sign_in' => ' zaloguj się, ',
    'to_follow_user' => ' by obserwować tego użytkownika.',
    'activities' => 'Ten użytkownik nie ma jeszcze żadnej aktywoności.'
];