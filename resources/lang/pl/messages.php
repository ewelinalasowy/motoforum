<?php


return[
    'success_create_thread' => 'Wątek utworzony prawidłowo!',
    'success_upload_thread' => 'Wątek edytowany prawidłowo!',
    'error_delete_thread' => 'Wątek usunięty prawidłowo.',
    'role_update_user' => 'Rola użytkownika zmieniona prawidłowo.',
    'deleted_user' => 'Użytkownik usunięty prawidłowo.',
    'category_added' => 'Nowa kategoria dodana pomyślnie.',
    'category_deleted' => 'Kategoria usunięta pomyślnie.',
    'success' => 'Sukces',
    'follow_yourself' => 'Nie możesz obserwowac samego siebie.',
    'friends_with' => 'Obserwujesz użytkownika :user',
    'already_following' => 'Obserwujesz już :user',
    'no_friends' => 'Nie jesteś przyjacielem z :user',
    'not_fallowing' => 'Nie jesteś obserwowany :user',
    'update_reply' =>'Odpowiedź zaaktualizowana pozytywnie',
    'delete_reply' => 'Odpowiedź została usunięta prawidłowo.'

];