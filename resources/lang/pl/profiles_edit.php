<?php


return [

    'edit' => 'Edycja profilu',
    'name' => 'Nazwa:',
    'mail' => 'E-mail: ',
    'submit' => 'Zatwierdź',
    'name_here' => 'Nazwa...',
    'mail_here' => 'E-mail...'
];