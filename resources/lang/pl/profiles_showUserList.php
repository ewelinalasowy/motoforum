<?php


return [

    'registered_users' => 'Lista zarejestrowanych użytkowników',
    'id' => 'ID',
    'name' => 'Nazwa',
    'mail' => 'E-mail',
    'activity' => 'Aktywność',
    'role' => 'Rola',
    'profile' => 'Pokaż profil',
    'submit' => 'Zatwierdź',
    'owner' => 'Właściciel',
    'delete' => 'Usuń',
    'list_of_roles' => 'Lista roli',
    'all' => 'Wszystkie'

];