<?php


return [

    'create' => 'Utwórz nowy wątek',
    'title' => 'Tytuł:',
    'body' => 'Treść:',
    'choose' => 'Wybierz kategorię:',
    'choose_one' => 'Wybierz jedną...',
    'publish' => 'Publikuj'
];