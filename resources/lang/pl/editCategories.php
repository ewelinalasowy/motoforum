<?php


return [

    'add_new_category' => 'Dodaj nową kategorię',
    'name' => 'Nazwa',
    'add' => 'Dodaj',
    'remove_category' => 'Usuń kategorię',
    'select_category_to_remove' => 'Wybierz kategorię do usunięcia',
    'question' => 'Jesteś pewien/a, że chcesz usunąć tę kategorię?',
    'delete' => 'Usuń'

];