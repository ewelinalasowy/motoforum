<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('locale/{locale}',function ($locale) {
   Session::put('locale',$locale);
   return redirect()->back();
});


Route::get('/home', 'HomeController@index')->name('home');

Route::any('/search','ThreadController@search');
Route::get('threads/create', 'ThreadController@create');
Route::get('threads', 'ThreadController@index');
Route::get('threads/{category}/{thread}', 'ThreadController@show');
Route::get('threads/{category}','ThreadController@index');
Route::get('/profiles/show/{user}', 'ProfileController@show')->name('profile');
Auth::routes(['verify'=>true]);
Route::get('/notifications', 'ProfileController@notifications');
Route::post('/profiles/show/{user}/follow', 'ProfileController@follow')->name('user.follow');
Route::post('/profiles/show/{user}/unfollow', 'ProfileController@unfollow')->name('user.unfollow');
Route::get('edit/threads/{category}/{thread}', 'ThreadController@edit');
Route::patch('edit/threads/{category}/{thread}', 'ThreadController@update');
Route::delete('threads/{category}/{thread}', 'ThreadController@destroy');
Route::delete('/replies/{reply}', 'ReplyController@destroy');
Route::get('threads/{category}/{thread}/replies/{reply}/edit', 'ReplyController@edit');
Route::patch('threads/{category}/{thread}/replies/{reply}/edit', 'ReplyController@update');
Route::post('/replies/{reply}/favorites', 'FavoriteController@store');
Route::post('threads', 'ThreadController@store');
Route::post('/threads/{category}/{thread}/replies', 'ReplyController@store');
Route::get('/profiles/edit', 'ProfileController@edit');
Route::patch('/profiles/edit', 'ProfileController@update');
Route::post('/threads/{category}/{thread}/subscriptions', 'ThreadSubscriptionController@store')->middleware('auth');
Route::delete('/threads/{category}/{thread}/subscriptions', 'ThreadSubscriptionController@destroy')->middleware('auth');


Route::group(['middleware'=>'admin'],function() {

    Route::get('/manage/users', 'ManageUsersController@showUserList')->name('Manage users');
    Route::post('/manage/users/delete/{id}', 'ManageUsersController@deleteUser')->name('Delete user');
    Route::patch('/manage/users/role', 'ManageUsersController@updateUserRole')->name('Change user access');
    Route::get('/manage/users/showByRoles/{roles}', 'ManageUsersController@showUsersByRoles')->name('Show users by role');
    Route::get('/manage/categories', 'ManageCategoriesController@showCategoryForm')->name('Manage categories');
    Route::post('/manage/categories/add', 'ManageCategoriesController@addCategory')->name('Add category');
    Route::post('/manage/categories/delete', 'ManageCategoriesController@deleteCategory')->name('Delete category');


});


