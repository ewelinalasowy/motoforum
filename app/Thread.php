<?php

namespace App;

use App\Notifications\ThreadWasUpdated;
use Illuminate\Database\Eloquent\Model;
use App\Filters\ThreadsFilter;
use Illuminate\Database\Eloquent\Builder;

class Thread extends Model
{


    protected $fillable = ['title','body','category_id','user_id','slug'];
    protected $appends = ['isSubscribedTo'];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('replyCounter',function($builder)
        {
            $builder->withCount('replies');
        });
        static::deleting(function ($thread){
            $thread->replies->each->delete();
        });

    }
    public function path()
    {
        return "/threads/{$this->category->slug}/{$this->slug}";
    }

    public function category()
    {
        return $this->belongsTo('App\Category','category_id');
    }

    public function replies()
    {
        return $this->hasMany('App\Reply');
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function addReply($replyRequest)
    {
        $reply = $this->replies()->create($replyRequest);

        $this->subscriptions
            ->filter(function ($sub) use ($reply) {
                return $sub->user_id != $reply->user_id;
            })
            ->each->notify($reply);
        return $reply;
    }

    public function scopeFilter($query, ThreadsFilter $filters)
    {
        return $filters->apply($query);
    }


    public function subscribe($userId = null)
    {
        $this->subscriptions()->create([
            'user_id' => $userId ?: auth()->id()
        ]);

        return $this;

    }

    public function unsubscribe($userId = null)
    {
        $this->subscriptions()
            ->where('user_id', $userId ?: auth()->id())
            ->delete();
    }

    public function subscriptions()
    {
        return $this->hasMany('App\ThreadSubscription');
    }

    public function getIsSubscribedToAttribute()
    {
        return $this->subscriptions()
            ->where('user_id', auth()->id())
            ->exists();
    }

    public function isSubscribed()
    {
        return !! $this->subscriptions()->where('user_id',auth()->id())->count();
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }



}
