<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class RolePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function delete(User $user, User $model)
    {
        if($user->role_id==1)
            return true;
        else
            return false;
    }

    public function update(User $user, User $model)
    {
        if($user->role_id==1 && $model->role_id==1)
            return false;
        else
            return true;
    }

}
