<?php

namespace App\Providers;

use Cache;
use Illuminate\Support\ServiceProvider;
use App\Category;
use View;
use App\Thread;
use App\Observers\ThreadObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        View::composer('*', function ($view) {
           $categories = Cache::rememberForever('categories',function(){
               return Category::all();
           });

               $view->with('categories',$categories);
        });

        Thread::observe(ThreadObserver::class);
    }
}
