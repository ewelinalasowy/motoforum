<?php

namespace App\Observers;
use App\Notifications\NewThread;
use App\Thread;

class ThreadObserver
{
    //

    public function created(Thread $thread)
    {
        $user = $thread->user;
        foreach ($user->followers as $follower) {
            $follower->notify(new NewThread($user, $thread));
        }
    }
}
