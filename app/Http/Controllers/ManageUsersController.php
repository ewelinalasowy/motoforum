<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Artesaos\SEOTools\Facades\SEOMeta;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;


class ManageUsersController extends Controller
{
    //

    public function showUserList(){

        SEOMeta::setTitle('Manage users');
        $userList= User::all();
        $roles = Role::all();
        return view('profiles.showUserList', compact('userList', 'roles'));
    }


    public function updateUserRole(Request $request){


        $user = User::where('id', $request->UID)->first();

        $user->role_id = $request->role_id;
        if ($user->role_id == 3) {
            $user->banned_until = Carbon::now()->addDays(14);
        }
        else
        {
            $user->banned_until = null;
        }

        $user->save();


        return redirect ('/manage/users')->with('success',trans('messages.role_update_user'));
    }





    public function deleteUser($id){
        $user=User::find($id);
        $user->delete();
        return redirect('/manage/users')->with('error',trans('messages.deleted_user'));
    }

    public function showUsersByRoles($id)
    {
        $roles = Role::all();
        $userList = User::all()->where('role_id', $id);
        return view('profiles.showUserList', compact('userList', 'roles'));
    }



}
