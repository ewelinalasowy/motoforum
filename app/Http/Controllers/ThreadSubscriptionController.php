<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Thread;

class ThreadSubscriptionController extends Controller
{


    public function store($categoryId, Thread $thread)
    {
        $thread->subscribe();
        return back();
    }


    public function destroy($categoryId, Thread $thread)
    {
        $thread->unsubscribe();
        return back();
    }
}
