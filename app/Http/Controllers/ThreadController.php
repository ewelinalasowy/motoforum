<?php

namespace App\Http\Controllers;

use Artesaos\SEOTools\Facades\SEOMeta;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use App\Thread;
use App\Category;
use App\Filters\ThreadsFilter;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use Illuminate\View\View;

class ThreadController extends Controller
{
    //

   public function __construct()
   {
       $this->middleware('auth') ->except(['index','show']);
   }

    public function index(Category $category, ThreadsFilter $filters)
  {

      SEOMeta::setTitle('Threads');
        $threads = $this->getThreads($category,$filters);

      return view('threads.index', compact('threads'));
  }

    /**
     * @param Thread $thread
     * @param integer $category
     * @return Factory|View
     */

  public function show($category, Thread $thread)
  {
      SEOMeta::setTitle($thread->title);
      return view('threads.show',[
          'thread' =>$thread,
          'replies' =>$thread->replies()->paginate(10)
      ]);
  }

  public function store (Request $request)
  {
      $this->validate($request, [
          'title' =>'required|unique:threads',
          'body' => 'required|max:1000',
          'category_id' => 'required'
      ]);



      $slug = Str::slug(request('title'));

      $thread = Thread::create([
          'user_id' => auth()->id(),
          'category_id' => request('category_id'),
          'title' => request('title'),
          'body' => request('body'),
          'slug'=> $slug,
      ]);


      return redirect($thread->path())->with('success',trans('messages.success_create_thread'));
  }

  protected function getThreads(Category $category, ThreadsFilter $filters)
  {
      $threads = Thread::latest()->filter($filters);

      if ($category->exists)
      {
          $threads->where('category_id',$category->id);
      }

      return $threads->get();
  }

  public function create()
  {
      SEOMeta::setTitle('Create thread');
      return view('threads.create');
  }

  public function destroy ($category, Thread $thread)
  {
      $this->authorize('delete', $thread);
      $thread->delete();
      return redirect('/threads')->with('error', trans('messages.error_delete_thread'));
  }

    public function edit(Category $category, Thread $thread)
    {

        return view('threads.edit',compact('thread'));
    }


    public function update(Request $request, Category $category, Thread $thread)
    {

        $this->validate($request, [
            'body' => 'required|max:1000',
            'category_id' => 'required',
        ]);


     $thread->body = $request->body;
     $thread->category_id = $request->category_id;

        $thread->save();

        $input = $request->only('body','category_id');
        $thread->update($input);


        return redirect('/')->with('success', trans('messages.success_update_thread'));

    }


    public function search()
    {
        $q = Input::get('q');
        if ($q != "") {
            $thread = Thread::where('title', 'LIKE', '%' . $q . '%')->get();
            if (count($thread) > 0)
                return view('search')->withDetails($thread)->withQuery($q);

        } else return view('search')->with('error', 'No threads found. Try to search again!');


        return redirect()->back()->with('error', 'No threads found. Try to search again!');
        }
}
