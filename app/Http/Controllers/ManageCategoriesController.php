<?php

namespace App\Http\Controllers;

use Auth;
use Artesaos\SEOTools\Facades\SEOMeta;
use Illuminate\Http\Request;
use App\Category;
use App\Thread;
use Illuminate\Support\Str;

class ManageCategoriesController extends Controller
{
    //

    public function showCategoryForm()
    {
        SEOMeta::setTitle('Manage categories');
        $categories=Category::all();
        return view('editCategories', ['categories'=>$categories]);
    }


    public function addCategory (Request $request) {
        $request->validate([
            'name' => 'required|max:50',
        ]);

        $category = new Category();
        $category->name = $request->name;
        $category->slug = Str::slug($category->name);

        tap($category)->save();

        return redirect('/manage/categories')->with('success',trans('messages.category_added'));
    }

    public function deleteCategory(Request $request){
        $category=Category::find($request)->first();
        $threads=Thread::where('category_id',$category->id)->get();

        foreach($threads as $thread)
        {
            $thread->delete();
        }

        $category->delete();
        return redirect('/manage/categories')->with('error',trans('messages.category_deleted'));
    }



}
