<?php

namespace App\Http\Controllers;

use App\Category;
use App\Thread;
use App\Reply;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;


class ReplyController extends Controller
{
    //



    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Thread $thread
     * @param integer $category_id
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function store ($category_id, Thread $thread)
    {
        $this->validate(request(),
            ['body' => 'required|max:1000']);

        $thread->addReply([
            'body'=> request('body'),
            'user_id' => auth()->id(),

        ]);

        return back();
    }

    public function destroy(Reply $reply)
    {
        $this->authorize('delete', $reply);
        $reply->delete();
        return back()->with('success',trans('messages.delete_reply'));
    }


    public function edit(Category $category, Thread $thread, Reply $reply)
    {
        return view('editReply',compact('reply','thread'));
    }


    public function update(Request $request,Category $category, Thread $thread, Reply $reply)
    {

        $this->validate($request, [
            'body' => 'required|max:1000',
        ]);


        $reply->body = $request->body;

        $reply->save();

        $input = $request->only('body');
        $reply->update($input);


        return redirect($thread->path())->with('success',trans('messages.update_reply'));

    }
}
