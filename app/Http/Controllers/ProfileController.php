<?php

namespace App\Http\Controllers;
use App\User;
use App\Thread;
use App\Activity;
use Artesaos\SEOTools\Facades\SEOMeta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Notifications\UserFollowed;
use Illuminate\Support\Facades\Lang;


class ProfileController extends Controller
{
    //

    public function show(User $user)
    {
        SEOMeta::setTitle($user->name);
        $followers = $user->followers;
        $followings = $user->followings;
        $threads =  Thread::where('user_id', '=',$user->id)->paginate(10);
        $profileUser = $user->name;
        $avatar = $user->avatar_name;


        return view('profiles.show', compact('profileUser', 'avatar','followers' , 'followings','threads'));
    }


    public function edit()
    {
        SEOMeta::setTitle('Edit profile');
        $user = Auth::user();
        return view('profiles.edit',compact('user'));
    }

    public function update(Request $request)
    {
        $user = Auth::user();

        $this->validate($request, [
            'name' => 'required|max:255|unique:users,name,'.$user->id,
            'email' => 'required|email|max:255|unique:users,email,'.$user->id,
            'avatar_name' => 'mimes:jpeg,png,jpg|max:2048',
        ]);


        if ($request->avatar_name!=NULL)
        {
            $avatarName = Auth::id().'_avatar' .'.'.$request->avatar_name->getClientOriginalExtension();
            $request->avatar_name->storeAs('avatars',$avatarName);
        }

        else
        {
            $avatarName = 'nophoto.png';
        }


        $user->avatar_name = $avatarName;



        $user->save();


        $input = $request->only('name','email');

        $user->update($input);



        return view('home')->with('success',trans('messages.success'));
    }

    public function follow(User $user)
    {
        $follower = auth()->user();
        if ($follower->id == $user->id) {
            return back()->with('error',trans('messages.follow_yourself'));
        }
        if(!$follower->isFollowing($user->id)) {
            $follower->follow($user->id);

            // sending a notification
            $user->notify(new UserFollowed($follower));

            return back()->with('success',trans('messages.friends_with',['user' => $user -> name]));
        }
        return back()->with('error',trans('messages.already_following',['user' => $user -> name]));
    }

    public function unfollow(User $user)
    {
        $follower = auth()->user();
        if($follower->isFollowing($user->id)) {
            $follower->unfollow($user->id);
            return back()->with('success',trans('messages.no_friends',['user' => $user -> name]));
        }
        return back()->with('error',trans('messages.not_fallowing', ['user' => $user -> name]));
    }


    public function notifications()
    {
        return auth()->user()->unreadNotifications()->limit(5)->get()->toArray();
    }
}
