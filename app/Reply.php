<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    //
    use Favorited;

    protected $fillable = ['body','user_id','thread_id',];


    public function thread()
    {
        return $this->belongsTo('App\Thread','thread_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function path()
    {
        return $this->thread->path() . "#reply-{$this->id}";
    }


}
